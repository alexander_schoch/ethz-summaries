\babel@toc {english}{}
\contentsline {section}{\numberline { \oldstylenums {1} }Logic, Sets and Numbers}{2}{section.1}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {1} .1} }Compositions}{2}{subsection.1.1}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {1} .2} }Quantors}{2}{subsection.1.2}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {1} .3} }Sets}{2}{subsection.1.3}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {1} .4} }Numbers}{3}{subsection.1.4}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {1} .4} .1} }Intervals}{3}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {1} .4} .2} }Bounds}{3}{subsubsection.1.4.2}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {1} .4} .3} }Extrema}{3}{subsubsection.1.4.3}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {1} .4} .4} }Suprema/Infima}{3}{subsubsection.1.4.4}
\contentsfinish 
