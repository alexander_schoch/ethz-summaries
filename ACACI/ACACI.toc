\babel@toc {english}{}
\contentsline {section}{\numberline { \oldstylenums {1} }General Knowledge}{3}{section.1}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {1} .1} }Chemical Equilibrium}{3}{subsection.1.1}
\contentsline {section}{\numberline { \oldstylenums {2} }Br\o nsted Acids and Bases}{3}{section.2}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {2} .1} }Definitions}{3}{subsection.2.1}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {2} .2} }Basics}{4}{subsection.2.2}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {2} .3} }Classification of Acids/Bases}{4}{subsection.2.3}
\contentsline {subsection}{\numberline { \oldstylenums { \oldstylenums {2} .4} }Helper Functions}{4}{subsection.2.4}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .1} }Protonenherkunftsgleichung}{4}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .2} }Massenerhaltung / Conservatoin of Mass}{5}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .3} }Calculations}{5}{subsubsection.2.4.3}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .4} }Strong Acids, Diluted}{5}{subsubsection.2.4.4}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .5} }Sttrong Acids, Concentrated}{5}{subsubsection.2.4.5}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .6} }Weak Acids, Diluted}{5}{subsubsection.2.4.6}
\contentsline {subsubsection}{\numberline { \oldstylenums { \oldstylenums { \oldstylenums {2} .4} .7} }Weak Acids, Concentrated}{5}{subsubsection.2.4.7}
\contentsline {section}{\numberline { \oldstylenums {3} }Redox Reactions}{5}{section.3}
\contentsline {section}{\numberline { \oldstylenums {4} }Complexes}{5}{section.4}
\contentsfinish 
