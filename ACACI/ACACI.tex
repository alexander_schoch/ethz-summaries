\input{/home/alexander_schoch/Templates/preamble.tex}
\input{/home/alexander_schoch/Templates/lstsetup.tex}
\input{/home/alexander_schoch/Templates/chemicalMacros.tex}

\usepackage{array}
\usetikzlibrary{arrows}

\setmonofont{CormorantGaramond}

\titel{General Chemistry: Inorganic Chemistry I}
\autor{Alexander Schoch}
\newcommand{\cmt}[1]{}




\date{\oldstylenums{Version: \today}}


\newcommand{\tplecturer}{Prof. Antonio Togni}
\newcommand{\tpabout}{\blindtext}

\begin{document}
  \input{../titlepage.tex}

  \newpage\tableofcontents\newpage

  \section{General Knowledge}

  \subsection{Chemical Equilibrium}

  The chemical Equilibrium can be expressed through the thermodynamical equilibrium constant, which is defined as 

  \begin{equation}
    K = \frac{\prod_{i=1}^{P}(a_i)^{e_i}}{\prod_{i=j}^{R}(a_j)^{e_i}}
  \end{equation}

  for number of products $P$, number of reactants $R$, indexes $i$ and $j$, stochiometric coefficients $e$ and activities $a$. Mentioned activities are defined as 

  \begin{equation}
    a_i = \gamma_i\frac{m_i}{m_i^0}
  \end{equation}

  for activity coefficient $\gamma$ and molalities $m$. Remember that the activity does not have a unit, which is why the equilibirium constant does not have one either. Note that instead of molalities, concentrations can be used, as one kilogram of water at $T = 298$ K and $p = 1$ atm (which is default in this lecture) is about one liter. For low concentrations ($c_i < 0.1$ mol L\hoch{-1}), activity coefficients $\gamma \approx 1$ and the difference between one kilogram of solvent and one kilogram of solution is neglible. Using this and the previous approximation, 

  \begin{equation}
    K = \frac{\prod_{i=1}^P(c_i)^{e_i}}{\prod_{i=j}^R(c_j)^{e_j}}
  \end{equation}

  can be used to calculate termodynamic equilibrium constants. For lower concentrations and \quotes{more ideal} substances (e.g. LiOSO\tief{2}NH\tief{2} (aq), whose $\gamma$ is nearly equal to one for all concentrations), this approximation will be more accurate.

  
  \section{Br\o nsted Acids and Bases}

  In Br\o nsted's definition of acid- and base reactions, protons are being transferred: 

  \begin{equation}
    \schemestart
      HA + B \arrow{<=>} A\hoch{-} + HB\hoch{+}
    \schemestop
  \end{equation}

  \subsection{Definitions}

  \paragraph{Acid} An acid is a substance which can (theoretically) act as a proton donor, meaning that it contains covalently bond hydrogen.
  \parmedskip

  \paragraph{Base} A base is a substance which can (theoretically) act as a proton acceptor, meaning that it has a non-bonding electron pair. \parmedskip

  \paragraph{Conjugated Acid} A conjugated acid is the acid form of a base. E.g. NH\tief{4}\hoch{+} is the conjugated acid of NH\tief{3}.\parmedskip

  \paragraph{Conjugated Base} A conjugated base is the base form of an acid. E.g. Cl\hoch{-} is the conjugated base of HCl.\parmedskip

  \paragraph{Ampholyte} A substance which can react both as a base and as an acid, e.g. H\tief{2}O, AcOH (acetic acid), etc.

  \subsection{Basics}

  We define a constant value $K_w$ which represents the correlation of the activities of OH\hoch{-} and H\hoch{+}.

  \begin{equation}
    K_w = a(H^+) * a(OH^-) \approx [H^+] * [OH^-] = 10^{-14}
  \end{equation}

  We can do the activity-to-concentration approximation, because the concentrations of both ions are (normally) very low. Now, we can define a pH value which shows the acidity of a solution: 

  \begin{equation}
    pH = -log([H^+]) = p([H^+])
  \end{equation}

  \paragraph{The \quotes{nivellierende Effekt}} means, that H\hoch{+} and OH\hoch{-} are the strongest acids/bases which can exist in water (\quotes{nivellierend} = \quotes{equilibrating}).

  \subsection{Classification of Acids/Bases}

  In order to get a number which characterizes the effect of an acid/base, a constant $K_a$ with 

  \begin{equation}
    K_a = K * [H_2O] = \frac{[H^+] * [A^-]}{[HA]}
  \end{equation}

  is defined, representing the potential of an \textit{acid} (not base). The reason for the special treatment of water is that the relative concentration of water is extremely high ($\approx 55.4$ mol L\hoch{-1}) and therefore we cannot use concentrations instead of activities. Nevertheless, it is possible to assume that the activity of water is constant, as the concentration of it barely changes relative to its actual concentration. Thus, this value is in principle merged with $K$ in order to get less constants. \absatz

  With a bit of rearranging, the Henderson-Hasselbalch equation (\quotes{Puffergleichung}) can be gotten:

  \begin{equation}
    pH = p\left(\frac{K_a * [HA]}{[A^-]}\right) = pK_a - log\frac{[HA]}{[A^-]} = pK_a + log\frac{[A^-]}{[HA]}
  \end{equation}

  Note that this equation cannot be used for strong acids ($pK_a < 1$), as a tiny change in $[HA]$ results in a huge change in the logarithm.

  \subsection{Helper Functions}

  First, a few variables have to be defined. 

  \begin{itemize}
    \item [HA]\tief{zg}: Added concentration of substance (\quotes{zugegeben})
    \item [A]\tief{tot}: Total amount of component A in whatever form it is (e.g. HA, H\tief{2}A, etc.)
  \end{itemize}

  <++>

  \subsubsection{Protonenherkunftsgleichung}

  This term refers to the principle that every proton has an origin, as you cannot just add protons into a solution. Those protons come from dissociated acids. Note that this also refers to autoprotosysis of water. Bases take protons form the solution, which means that species which reacted as a base have to be subtracted. \parmedskip

  \paragraph{Example} Sodium Bicarbonate (NaHCO\tief{3}) has been given into water. 

  \begin{equation}
    [H^+] = [CO_{3}^-] + [OH^-] - [H_2CO_3]
  \end{equation}

  
  \subsubsection{Massenerhaltung / Conservatoin of Mass}

  This term refers to the principle that no mass can vanish. Therefore, the amount of X in a solution either comes form added X, or protonated/deprotonated forms of it. \parmedskip

  \paragraph{Example} Sodium Bicarbonate (NaHCO\tief{3}) has been given into water.

  \begin{equation}
    [HCO_3^-]_{zg} = [CO_3^{2-}]_{tot} = [H_2CO_2] + [HCO_3^-] + [CO_3^{2-}]
  \end{equation}

  
  \subsubsection{Calculations}

  With those four functions (Massenerhaltung, Protonenherkunftsgleichung, $K_w$, $K_a$), it is now possible to calculate the pH value of solutions. \absatz

  In the following sections, a monoprotic example acid HA will be used. 

  \subsubsection{Strong Acids, Diluted}

  \begin{equation}
    \begin{array}{rll}
      [H^+] &= [A^-] + [OH^-] \\
      &= [HA]_{zg} + \frac{K_w}{[H^+]} \\
      &= \frac{[HA]_{zg} \pm \sqrt{[HA]_{zg}^2 + 4 * K_w}}{2}
    \end{array}
  \end{equation}

  \subsubsection{Sttrong Acids, Concentrated}

  \begin{equation}
    \begin{array}{rll}
      -log([H^+]) &= -log([A^-] + [OH^-]) & \text{Protonenherkunftsgleichung} \\
      &= -log([A^-]) & \text{[OH\hoch{-}] is negligible, $[OH^-] \approx 0$} \\
      &= -log([HA]_{zg}) & \text{All the added acid is dissociated, $[A^-] >> [HA]$}
    \end{array}
  \end{equation}

  \subsubsection{Weak Acids, Diluted}

  \begin{equation}
    \begin{array}{rll}
      \left[ H^+ \right] &= \frac{-K_a + \sqrt{K_a^2 + 4 * [HA]_{zg} * K_a}}{2} \\
    \end{array}
  \end{equation}


  \subsubsection{Weak Acids, Concentrated}

  \begin{equation}
    \begin{array}{rll}
      pH = \frac{1}{2}\left(pK_a - log[HA]_{zg} \right)
    \end{array}
  \end{equation}

  

  \section{Redox Reactions}
  \section{Complexes}

  
  \bibliographystyle{unsrt}
  \bibliography{literature}
  \computerFont
\end{document}
